Notepod is a note-taking app that saves notes to your [Solid Pod](https://solidproject.org).

It was created to assist in learning how to write Solid apps. Follow [the tutorial](https://vincenttunru.gitlab.io/tripledoc/docs/writing-a-solid-app/writing-a-solid-app), or read the commits of this repository.
